# ZAP

ZAP is a CMS that lets you edit locally and deploy your site using git.

## Install

To start a new site:

 1. copy the site.json and serve-local scripts to a new folder
 2. set up that folder as a git repo (`git init .` etc..)
 3. clone zap somewhere on your computer
 4. set the config.local.ZAP\_PATH variable in site.json to the folder you cloned zap into
 5. set up a virtualenv
    1. `virtualenv env`
    2. `. env/bin/activate`
    3. `pip install -r <path_to_zap>/requirements.txt`

Alright! It's all set up for local development.


## Local Editing

Zap uses web technologies to let you edit your posts locally using a convenient browser-based interface. To launch the interface, just run

    . env/bin/activate && ./serve-local edit
Then, open up [http://localhost:5000](http://localhost:5000) in your browser to access the editor.

## Local Preview
Zap also lets you do a local preview without having to set up any server on your development machine. You should not use this serve to serve to the internet at large.

    . env/bin/activate && ./serve-local preview

This also serves on [http://localhost:5000](http://localhost:5000).

## Deployment
To deploy your site, first follow the install instructions on your server. Set the config.deploy.ZAP\_PATH instead of config.local.ZAP\_PATH.

ZAP uses Python and Flask to create a WSGI app that can be served using a WSGI server. For details on setting one up, you can follow the
[Flask documentation for deploying uWSGI](http://flask.pocoo.org/docs/deploying/uwsgi/).

When it is given no arguments, the serve-local script in your project will export a WSGI app in the variable `app`.

## ElasticSearch support
ZAP uses ElasticSearch (mandatory, for now) to search your posts both in the
editor and the server mode. It should be configured with an Elastic Search
index that it will own completely. It will delete and recreate the index every
time it starts, so you should not be using it for any other purpose.
