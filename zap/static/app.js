angular.module("zapApp", ['ngResource', 'ngRoute', 'slugifier'])
	.factory('PostModel', function($resource, $http) {
		var Post = $resource('/post/:postId', {postId: '@id'}, {
			create: {method: 'GET' ,url: '/post/new', cache: false}
		});

		Post.addPhoto = function(post, img) {
			var url = '/post/' + post.id + '/add_image'
				,fd = new FormData();
			fd.append('file', img);

			return $http.post(url, fd, {
				transformRequest: angular.identity
				,headers: {'Content-Type': undefined}
			}).then(function(result) {
				post.images.push(result.data);
			});
		};

		return Post;
	})

	.controller('MainCtrl', function($scope) {
	})

	.config(function($routeProvider, $locationProvider) {
		// $locationProvider.html5Mode(true);

		$routeProvider.when('/list', {
			templateUrl: '/static/partials/list.html'
			,controller: 'ListCtrl'
			,resolve: {
				posts: function($route, PostModel) {
					return PostModel.query({q:$route.current.params.q}).$promise;
				}
			}
		});
		$routeProvider.when('/edit/:id', {
			templateUrl: '/static/partials/edit-post.html'
			,controller: 'PostCtrl'
			,resolve: {
				post: function($route, PostModel) {
					return PostModel.get({postId: $route.current.params.id}).$promise;
				}
			}
		});
		$routeProvider.when('/new', {
			templateUrl: '/static/partials/edit-post.html'
			,controller: 'PostCtrl'
			,resolve: {
				post: function($route, PostModel) {
					return PostModel.create().$promise;
				}
			}
		});
		$routeProvider.otherwise({redirectTo: '/list'});
	})

	.controller('ListCtrl', function($scope, $routeParams, $route, $sce, $location) {
		$scope.posts = $route.current.locals.posts;
		angular.forEach($scope.posts, function(post) {
			post.summary = $sce.trustAsHtml(post.summary);
		});

		$scope.search = function() {
			$location.search('q', $scope.search.query);
		};
		$scope.search.query = $routeParams.q;
		$scope.search.old_query = $routeParams.q;
	})

	.controller('PostCtrl', function($scope, $route, $location, PostModel) {
		$scope.post = $route.current.locals.post;

		$scope.save = function() {
			$scope.post.$save();
		};

		$scope.toggleFavorite = function() {
			$scope.post.fave = !$scope.post.fave;
		};

		$scope.delete = function() {
			$scope.post.$delete();
			$location.path('/');
		};

		$scope.addImage = function(file) {
			PostModel.addPhoto($scope.post, file);
		};

		$scope.rmImage = function(index) {
			$scope.post.images.splice(index, 1);
		};
	})

;
