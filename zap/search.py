import uuid

import elasticsearch, elasticsearch.helpers
import markdown




class Search(object):
	def __init__(self, site, collection):
		# TODO: load these from configuration
		self._index = site.config['search']['index']
		self._type = 'post'

		self.site = site

		hosts = site.config['search'].get('hosts', ["localhost:9200"])
		self.client = elasticsearch.Elasticsearch(hosts)
		self.collection = collection
		self.collection.add_listener(self)

		for post in self.collection.all():
			post.add_listener(self)

		self.setup_index()
		self.put_all_posts()


	def search_ids(self, query_string):
		"""get all document ids that match this query"""
		result = self.client.search(index=self._index, doc_type=self._type,
			fields=["id"], size=len(self.collection.all()),
			body={
				'query': {
					'query_string': {
						'default_field': '_all'
						,'query': query_string
					}
				}
			})

		ids = [self.un_es_id(hit['_id']) for hit in result['hits']['hits']]
		return ids


	def index(self, post):
		self.client.index(index=self._index, doc_type=self._type,
			id=self.es_id(post), body=self.rendered_json(post))

	def put_all_posts(self):
		elasticsearch.helpers.bulk(self.client, [
			{
				'_index': self._index
				,'_type': self._type
				,'_id': self.es_id(post)
				,'_source': self.rendered_json(post)
			}
		for post in self.collection.all()])

	def setup_index(self):
		self.client.indices.delete(index=self._index ,ignore=[404, 400])
		self.client.indices.create(index=self._index, body={
			"mappings": {
				self._type: {
					"_source": {"enabled": False}
					,"properties": {
						"content": {
							"type": "string"
							,"char_filter": ["html_strip"]
						}
					}
				}
			}
		})

	@staticmethod
	def es_id(post):
		"""makes a hexadecimal, url safe version of post.id"""		
		return post.id.int

	@staticmethod
	def un_es_id(es_id):
		return uuid.UUID(int=int(es_id))

	def rendered_json(self, post):
		"""makes a dict from post with title, labels, etc. and html content
			all searchy stuff will be lowercased"""
		return {
			'title': post.title
			,'labels': post.labels
			,'content': markdown.markdown(post.content or '')
			,'timestamp': post.timestamp
		}


	# Observer callbacks
	def post_added(self, post):
		"""callback from observing self.collection"""
		self.index(post)
		post.add_listener(self)

	def post_deleted(self, post):
		"""callback from observing self.collection"""
		self.delete_post(self, post)
		# TODO: do we need to stop observing the post here?

	def post_updated(self, post):
		"""callback from observing individual posts"""
		self.index(post)
