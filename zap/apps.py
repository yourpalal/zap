#!/usr/bin/env python3


import flask, os, uuid

from flask import Flask, Response, redirect, request, safe_join, \
					 send_file, url_for
from werkzeug import secure_filename


from .controllers import PostController
from .models import Post, SiteModel
from .views import AtomView, JsonView, TemplatedView
from .search import Search
from .storage import  SCSS



class BaseApp(object):
	def __init__(self, site, postController):
		super().__init__()
		self.site = site
		self.folder = site.folder
		self.postController = postController
		self.scss = SCSS(self.site)
		self.search = Search(self.site, self.postController.posts)

	def serve_resource(self, resource):
		ext = os.path.splitext(resource)[1].lower()
		resource_folder = os.path.join(self.folder, 'html')

		if ext == '.css':
			return Response(self.scss.compile(resource), mimetype='text/css')
		else:
			return send_file(safe_join(resource_folder, resource))


class ServeApp(BaseApp):
	def __init__(self, site, flask_app):
		super().__init__(site, PostController.for_folder(site.folder))

		self.atomView = AtomView(flask_app, self.site)
		self.htmlView = TemplatedView(self.folder, {
			'image': '/{post_slug}/{image}',
			'atom': '/{slug}.atom'
		}, {
			'img_src': self.img_src_hook
		}, auto_reload=True)

		self.defineRoutes(flask_app)

	def img_src_hook(self, post, img, w=0, h=0):
		self.postController.storage.make_thumbnail(post, img, (w, h))

	def defineRoutes(self, flask_app):
		@flask_app.route("/")
		def main():
			if (self.site.index and self.site.index != '/'):
				return redirect(os.path.join('/', self.site.index))
			else:
				posts = self.postController.all()
				return self.htmlView.render_list(posts=posts,
					breadcrumbs=self.postController.get_breadcrumbs(''))

		@flask_app.route("/<slug>.atom", methods=['GET'])
		def posts_atom(slug):
			post = self.postController.posts.find_one(Post.match_slug(slug))
			if not post:
				posts = self.postController.get_posts_by_labels(labels=[slug])
				if posts: return self.atomView.render_list(slug, posts)
			flask.abort(404)

		# post routes
		@flask_app.route("/<slug>", methods=['GET'])
		def view_post(slug):
			post = self.postController.posts.find_one(Post.match_slug(slug))
			if post:
				return self.htmlView.render_post(post=post,
					breadcrumbs=self.postController.get_breadcrumbs(post))

			posts = self.postController.get_posts_by_labels(labels=[slug])
			if posts:
				return self.htmlView.render_list(posts=posts,
						breadcrumbs=self.postController.get_breadcrumbs(slug))
			flask.abort(404)

		@flask_app.route("/search", methods=['GET'])
		def search_posts():
			query = request.args.get('q', '')
			posts = self.postController.search(query, self.search)
			return self.htmlView.render_search_results(posts=posts,
					search_query=query,
					breadcrumbs=self.postController.get_breadcrumbs(posts))

		@flask_app.route("/<post_slug>/<image_id>", methods=['GET'])
		def view_img(post_slug, image_id):
			w = request.args.get('w')
			h = request.args.get('h')

			return self.postController.view_image(Post.match_slug(post_slug),
													image_id, w, h)
			
		# misc routes
		@flask_app.route("/resource/<resource_name>", methods=['GET'])
		def get_resource(resource_name):
			return self.serve_resource(resource_name)


class EditApp(BaseApp):
	def __init__(self, site, flask_app):
		super().__init__(site, PostController.for_folder(site.folder))
		self.htmlView = TemplatedView(self.folder, {
			'image': '/post/{post_id}/image/{image}'
			,'atom': '/{slug}.atom'
		}, auto_reload=True)
		self.jsonView = JsonView()

		self.defineRoutes(flask_app)

	def defineRoutes(self, flask_app):
		@flask_app.route("/")
		def main():
			return redirect(url_for('static', filename='index.html'))

		# post routes
		@flask_app.route("/post/<post_id>", methods=['GET', 'POST'])
		def update_post(post_id):
			post_matcher = Post.match_id(post_id)
			if request.method == 'GET':
				post = self.postController.posts.find_one(post_matcher)
			elif request.method == 'POST':
				post = self.postController.update_post(post_matcher,
							request.get_json())
			return post.to_json()

		@flask_app.route("/post/<post_id>", methods=['DELETE'])
		def delete_post(post_id):
			self.postController.delete_post(Post.match_id(post_id))
			return 'post deleted'

		@flask_app.route("/post/new")
		def new_post():
			post = self.postController.create_post()
			return post.to_json()

		@flask_app.route("/post/")
		def list_posts():
			query = request.args.get('q', '')
			if query:
				posts = self.postController.search(query, self.search)
			else:
				posts = self.postController.all()
			return self.jsonView.json_posts_summary(posts)

		@flask_app.route("/post/<post_id>/preview", methods=['GET'])
		def preview(post_id):
			post = self.postController.posts.find_one(Post.match_id(post_id))
			return self.htmlView.render_post(post=post,
						breadcrumbs=self.postController.get_breadcrumbs(post))
			
		# image routes
		@flask_app.route("/post/<post_id>/add_image", methods=['POST'])
		def add_image(post_id):
			f = request.files['file']
			img = self.postController.add_image(Post.match_id(post_id),
				secure_filename(f.filename), f)
			return self.jsonView.json_image(img)

		@flask_app.route("/post/<post_id>/image/<image_id>", methods=['GET'])
		def view_image(post_id, image_id):
			return self.postController.view_image(Post.match_id(post_id),
													image_id)

		# misc routes
		@flask_app.route("/resource/<resource_name>", methods=['GET'])
		def get_resource(resource_name):
			return self.serve_resource(resource_name)

		@flask_app.route("/list", methods=['GET'])
		def get_List():
			return self.htmlView.render_list(posts=self.postController.all(),
				breadcrumbs=self.postController.get_breadcrumbs(''))
