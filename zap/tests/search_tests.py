from ..models import Post
from ..search import Search

import datetime, json, random, unittest, unittest.mock, uuid

from .common import PostFactory


class TestElasticSearch(unittest.TestCase):
	def setUp(self):
		pass

	def test_id_roundtrip(self):
		post = Post()
		es_id = Search.es_id(post)
		self.assertEquals(Search.un_es_id(es_id), post.id)
		self.assertEquals(Search.un_es_id(str(es_id)), post.id)
