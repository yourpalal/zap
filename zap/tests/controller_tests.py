from ..controllers import PostController
from ..storage import PostsStorage
from ..models import Post


import datetime, unittest, unittest.mock, uuid


class TestPostController(unittest.TestCase):
	def setUp(self):
		self.post = Post().update({
			'labels': ['cool', 'awesome']
		})

		storage = unittest.mock.Mock(PostsStorage)
		storage.find_all.return_value = iter([self.post])
		self.controller = PostController(storage)

	def test_get_breadcrumbs_for_label(self):
		breadcrumbs = self.controller.get_breadcrumbs('awesome')

		self.assertEqual(len(breadcrumbs), 2)
		for b in breadcrumbs:
			self.assertEqual(b['active'], b['name'] == 'awesome')

		self.assertEqual({'cool', 'awesome'}, {b['name'] for b in breadcrumbs})

	def test_get_breadcrumbs_for_post(self):
		breadcrumbs = self.controller.get_breadcrumbs(self.post)

		self.assertEqual(len(breadcrumbs), 2)
		for b in breadcrumbs:
			self.assertTrue(b['active'])
		self.assertEqual({'cool', 'awesome'}, {b['name'] for b in breadcrumbs})
