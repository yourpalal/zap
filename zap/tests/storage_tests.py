from ..storage import PostsStorage, SinglePostStorage
from ..models import ImageAttachment, Post

from .common import PostFactory

import datetime, os, shutil, tempfile, unittest


class ImageCopier(object):
	"""For testing saving images with PostsStorage"""
	def __init__(self, src):
		self.src = src

	def save(self, dest):
		os.makedirs(os.path.split(dest)[0], exist_ok=True)
		shutil.copy(self.src, dest)


class TestPostsStorage(unittest.TestCase):
	def setUp(self):
		self.date = datetime.datetime(year=2012, month=1, day=1)
		self.folder = os.path.join(os.path.split(__file__)[0], "data")
		self.storage = PostsStorage(self.folder)
		self.post = PostFactory().make_post(storage=self.storage)

	def test_get_folder_for_post_is_pure(self):
		folder = self.storage.get_folder_for_post(self.post)
		assert folder
		assert folder == self.storage.get_folder_for_post(self.post), \
			"folder changed"

	def test_get_folder_for_post_changes_with_timestamp(self):
		folder = self.storage.get_folder_for_post(self.post)
		self.post.timestamp = datetime.datetime.now().timestamp()
		assert folder != self.storage.get_folder_for_post(self.post), \
			"folder didn't change with timestamp"

	def test_get_folder_for_post_uses_id(self):
		folder = self.storage.get_folder_for_post(self.post)
		assert str(self.post.id) in folder, \
			"id not in path"

	def test_get_all_posts(self):
		"""(integration) can get all posts in the storage folder
			with find_all()"""
		posts = list(self.storage.find_all())
		self.assertIsNotNone(posts)
		self.assertEqual(len(posts), 2, "there should be two posts")

	def test_save_post_with_image(self):
		"""(integration) can save a post that uses an image"""
		def test_in_dir(tmp_dir):
			self.copy_image(self.post, 'fake.png')
			self.storage.save(self.post)

			posts = list(self.storage.find_all())
			self.assertIsNotNone(posts)
			self.assertEqual(len(posts), 1, "there should be one post")
			self.assertEqual(posts[0].images[0].name, 'fake.png')
			self.assertEqual(posts[0].images[0].caption, 'fake.png')
		self.integrate_with_temp_dir(test_in_dir)

	def test_save_post(self):
		"""(integration) can round-trip a new post on the disk"""
		def test_in_dir(tmp_dir):
			self.storage.save(self.post)

			posts = list(self.storage.find_all())
			self.assertIsNotNone(posts)
			self.assertEqual(len(posts), 1, "there should be one post")

			post = posts[0]
			compare = ['content', 'title', 'timestamp', 'slug', 'id', 'fave']
			for attr in compare:
				self.assertEqual(getattr(post, attr), getattr(self.post, attr),
					"{} should be preserved after round-trip".format(attr))
			self.assertEqual(set(post.labels), set(self.post.labels),
					"labels should be preserved after round-trip")
		self.integrate_with_temp_dir(test_in_dir)

	def test_clean_post_images(self):
		"""(integration) can clean up now-unused images for a post"""
		def test_in_dir(tmp_dir):
			self.copy_image(self.post, 'fake.png', 'remove.png')
			keep = self.copy_image(self.post, 'fake.png', 'keep.png')
			self.storage.save(self.post)

			cleaned = []
			def clean_image(post, image):
				cleaned.append(image)
			self.storage.clean_images(self.post, [keep],
										inject=clean_image)

			print(cleaned)
			self.assertIn('remove.png', cleaned)
			self.assertEqual(len(cleaned), 1)
	
		self.integrate_with_temp_dir(test_in_dir)	

	# TODO: this could be done as a decorator
	def integrate_with_temp_dir(self, action):
		"""run an integration test in a temporary directory"""
		with tempfile.TemporaryDirectory() as tmp:
			self.storage = PostsStorage(str(tmp))
			self.assertEqual(len(list(self.storage.find_all())), 0,
				"should find no posts in a new directory")

			action(tmp)

	def copy_image(self, post, image_name, final_name=None):
		"""Add image_name from self.folder as an attachment to post,
			with the name final_name, or image_name by default"""
		final_name = final_name or image_name
		fake_image = ImageCopier(os.path.join(self.folder, image_name))
		img = self.storage.save_image(self.post, final_name, fake_image)
		img.caption = image_name
		return img
