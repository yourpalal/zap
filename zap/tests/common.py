from ..models import Post
from ..storage import SinglePostStorage

import datetime, random


alphabet = "".join([chr(i) + chr(i).upper() for i in range(ord('a'), ord('z'))])
text_sample = " \n"  + alphabet


class PostFactory(object):
	"""Utility class for making posts with random/sequential data for
		testing"""

	RAND = 1
	SEQ = 2

	def __init__(self, content=RAND, timestamp=SEQ, fave=RAND, slug=SEQ,
					labels=None, title=SEQ):
		"""Make a post factory that will generate posts according to a spec.
			content, timestamp, fave, title, and slug can have values of
			RAND, SEQ, or None, which will produce random, sequential, or no
			values, respectively.

			labels can have an array of RAND, SEQ, and (int -> string) values"""
		default_labels = [PostFactory.SEQ, PostFactory.even_odd]

		self.content = content
		self.timestamp = timestamp
		self.fave = fave
		self.labels = labels if labels is not None else default_labels
		self.title = title
		self.slug = slug

		self.i = -1

	def make_posts(self, count, storage=None):
		return [self.make_post(storage) for i in range(0, count)]

	def make_post(self, storage=None):
		storage = SinglePostStorage(storage) if storage else None
		self.i += 1
		return Post().update({
			'content': self.make_content()
			,'title': self.make_title()
			,'slug': self.make_slug()
			,'fave': self.make_fave()
			,'timestamp': self.make_timestamp()
			,'labels': list(self.make_labels())
		})
		
	def make_content(self):
		if self.content == PostFactory.RAND:
			return "".join(random.choice(text_sample) for i in range(200))
		elif self.content == PostFactory.SEQ:
			return "this is the sequential blog post number {}".format(self.i)

	def make_title(self):
		if self.title == PostFactory.RAND:
			return "".join(random.choice(alphabet) for i in range(20))
		elif self.title == PostFactory.SEQ:
			return "this is post {}".format(self.i)

	def make_slug(self):
		if self.slug == PostFactory.RAND:
			return "".join(random.choice(alphabet) for i in range(20))
		elif self.slug == PostFactory.SEQ:
			return "post-{}".format(self.i)
		
	def make_fave(self):
		if type(self.fave) is bool:
			return self.fave
		elif self.fave == PostFactory.RAND:
			return random.choice((True, False))
		elif self.fave == PostFactory.SEQ:
			return True if i % 2 else False

	def make_timestamp(self):
		if self.timestamp == PostFactory.RAND:
			return datetime.datetime(year=random.randint(1990, 2080),
					month=random.randint(0,11), day=random.randint(0, 28))
		elif self.timestamp == PostFactory.SEQ:
			return datetime.datetime(year=1990 + self.i, month=self.i % 12 + 1,
															day=self.i%28 + 1)

	def make_labels(self):
		for label in self.labels:
			if label == PostFactory.RAND:
				yield "".join(random.choice(alphabet) for i in range(20))
			elif label == PostFactory.SEQ:
				yield "label{}".format(self.i)
			elif type(label) is str:
				yield label
			elif label is not None:
				result = label(self.i)
				if result is not None: yield result
			
	@staticmethod
	def even_odd(i):
		return "odd" if i % 2 else "even"
