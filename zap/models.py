import itertools, json, markdown, os, re, uuid

from datetime import datetime


class PostCollection(object):
	def __init__(self, posts):
		self.posts = posts
		self.listeners = []

	def all(self):
		return self.posts

	def add(self, post):
		self.posts.append(post)
		for listener in self.listeners: listener.post_added(post)

	def delete(self, post):
		self.posts = [p for p in self.posts if p.id != post.id]
		for listener in self.listeners: listener.post_deleted(post)

	def find_one(self, matcher):
		posts = [p for p in self.posts if matcher.matches(p)]
		return posts[0] if posts else None

	def find(self, labels):
		labels = set(labels)
		has_labels = lambda p: len(labels - set(p.labels)) == 0 
		return [p for p in self.posts if has_labels(p)]

	def find_by_id(self, *ids):
		ids = itertools.chain(*ids)
		return [self.find_one(Post.match_id(post_id)) for post_id in ids]

	def add_listener(self, listener):
		self.listeners.append(listener)


class Post(object):
	class Matcher(object):
		def __init__(self, id, slug):
			self.id = (uuid.UUID(id) if type(id) == str else id)
			self.slug = slug

		def matches(self, post):
			if self.id and post.id != self.id: return False
			if self.slug and post.slug != self.slug: return False

			return True

	@staticmethod
	def match_slug(slug):
		return Post.Matcher(None, slug)

	@staticmethod
	def match_id(id):
		return Post.Matcher(id, None)


	def __init__(self, id=None, storage=None):
		self.id = (uuid.UUID(id) if type(id) == str else id)
		self.storage = storage
		if self.storage:
			self.storage.set_post(self)
		self.slug = None
		self.content = None
		self.setDefaults()
		self.listeners = []

	def __str__(self):
		return "{} {}".format(self.title, str(self.id))

	def setDefaults(self):
		defaults = {
			'title': 'untitled draft',
			'id': uuid.uuid4(),
			'timestamp': datetime.now().timestamp(),
			'fave': False,
			'images': [],
			'labels': []
		}
		for key, val in defaults.items():
			if not hasattr(self, key) or not getattr(self, key):
				setattr(self, key, val)
		return self

	def summary(self):
		raw = self.content or ''
		return re.split(r'<hr\s*/?>', markdown.markdown(raw), 1)[0]

	def to_json(self):
		for_json = ['content', 'title', 'timestamp', 'slug', 'id']
		safe_str = lambda x: str(x) if x is not None else ''
		data = {k: safe_str(getattr(self, k)) for k in for_json}
		data['fave'] = self.fave
		data['labels'] = self.labels
		data['images'] = [img.for_json() for img in self.images]
		return json.dumps(data)

	def update(self, data):
		"""update from the same format as produced in to_json()"""

		update = ['content', 'title', 'timestamp', 'fave', 'labels', 'slug']
		for attr in update:
			if attr in data:
				setattr(self, attr, data[attr])

		if type(self.timestamp) is str:
			self.timestamp = float(self.timestamp) 
		elif type(self.timestamp) is datetime:
			self.timestamp = self.timestamp.timestamp()

		self.labels = list(set(self.labels))
		if self.storage:
			self.images = self.storage.update_images(data.get('images', []))

		self.setDefaults()
		for listener in self.listeners:
			listener.post_updated(self)

		return self

	def add_image(self, img):
		self.images.append(img)
		return img

	def find_image(self, name):
		images = [i for i in self.images if i.name == name]
		return images[0] if images else None

	def add_listener(self, listener):
		self.listeners.append(listener)


class ImageAttachment(object):
	def __init__(self, name, size):
		self.name = name
		self.caption = ''
		self.size = size

	def to_json(self):
		return json.dumps(self.for_json())

	def for_json(self):
		return {
			'name': self.name,
			'size': self.size,
			'caption': self.caption,
		}

	def get_thumbnail_name(self, size):
		size = self.normalize_size(size)
		filename = os.path.splitext(self.name)
		return '{}{}x{}{}'.format(filename[0],
				size[0], size[1], filename[1])

	def normalize_size(self, size):
		width, height = size
		if width:
			height = int((width / self.size[0]) * self.size[1])
		elif height:
			width = int((height / self.size[1]) * self.size[0])
		else:
			return self.size
		return (width, height)


class SiteModel(object):
	FILENAME = 'site.json'

	def __init__(self, folder, deployed=False):
		self.folder = folder
		self.deployed = deployed

		try:
			with open(os.path.join(self.folder, SiteModel.FILENAME)) as f:
				data = json.load(f)
				self.update(data)
		except FileNotFoundError:
			self.domain = ''
			self.index = ''

			self.save()

	def save(self):
		with open(os.path.join(self.folder, SiteModel.FILENAME), 'w') as f:
			json.dump(self.to_json(), f)

	def update(self, data):
		self.domain = data['domain']
		self.index = data['index']
		self.config = data['config']['deployed' if self.deployed else 'local']

	def to_json(self):
		return {'domain': self.domain, 'index': self.index}
