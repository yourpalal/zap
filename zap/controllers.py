#!/usr/bin/env python3


import itertools, uuid

from flask import Flask, url_for, redirect, request, safe_join, send_file

from .models import Post, PostCollection
from .views import PostSorter
from .storage import PostsStorage


class PostController(object):
	def __init__(self, storage):
		self.storage = storage
		self.posts = PostCollection(list(self.storage.find_all()))
		self.sorter = PostSorter()

	@staticmethod
	def for_folder(location):
		return PostController(PostsStorage(location))

	def delete_post(self, matcher):
		post = self.posts.find_one(matcher)
		self.posts.delete(post)
		self.storage.delete(post)

	def create_post(self):
		post = Post()
		self.storage.save(post)
		self.posts.add(post)
		return post

	def update_post(self, post_matcher, data):
		post = self.posts.find_one(post_matcher)
		post.update(data)
		self.storage.save(post)
		return post

	def all(self):
		return self.sorter.sort(self.posts.all())

	def search(self, query, search):
		ids = search.search_ids(query)
		return self.posts.find_by_id(ids)

	def get_posts_by_labels(self, labels):
		posts = self.posts.find(labels=labels)
		return self.sorter.sort(posts)

	def get_breadcrumbs(self, active):
		labels = set.union(*[set(post.labels) for post in self.posts.all()])
		if type(active) is str:
			active = set([active])
		elif type(active) is Post:
			# generate for a post
			active = set(active.labels)
		elif type(active) is list:
			# generate for a bunch of posts
			labels_generator = (post.labels for post in active) 
			active = set(itertools.chain.from_iterable(labels_generator))

		return [{'name': l, 'location': '/' + l, 'active': l in active}
					for l in sorted(labels)]

	def add_image(self, post_matcher, name, img_data):
		post = self.posts.find_one(post_matcher)
		return self.storage.save_image(post, name, img_data)

	def view_image(self, post_matcher, image_id, w=None, h=None):
		"""view an image or thumbnail"""
		post = self.posts.find_one(post_matcher) or flask.abort(404)
		image = post.find_image(image_id) or flask.abort(404)

		w = int(w) if w else None
		h = int(h) if h else None
		if w or h: 
			path = self.storage.thumbnail_path(post, image, (w,h))
		else:
			path = self.storage.image_path(post, image)
		return send_file(path)
