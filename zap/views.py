#!/usr/bin/env python3


import datetime, jinja2, json, markdown, os, re

from flask import request
from urllib.parse import urljoin
from werkzeug.contrib.atom import AtomFeed


class PostSorter(object):
	"""Sorts posts by date/favourites"""
	def __init__(self):
		pass

	def sort(self, posts):
		return sorted(posts, reverse=True, key=self.get_sort_key)

	def get_sort_key(self, post):
		return (post.fave, post.timestamp)


class JsonView(object):
	def __init__(self):
		pass

	def json_posts_summary(self, posts):
		return json.dumps([
			self.post_summary(post) for post in posts
		])

	def post_summary(self, post):
		return {
			'id': str(post.id),
			'title': post.title,
			'fave': post.fave,
			'timestamp': post.timestamp,
			'summary': post.summary()
		}

	def json_image(self, image):
		return json.dumps(image.for_json())


class AtomView(object):
	def __init__(self, app, site):
		self.site = site

	def render_list(self, name, posts):
		feed = AtomFeed(name, feed_url=request.url, url=self.site.domain)
		for post in posts:
			feed.add(post.title,
				str(''.join(re.split(r'<hr\s*/?>',
					markdown.markdown(post.content['body'])))),
				content_type='html',
				author='author',
				url=urljoin(self.site.domain, post.slug),
				updated=datetime.datetime.fromtimestamp(post.timestamp))
		return feed.get_response()

	def url_filter(self, post):
		return os.path.join(self.site.domain, post.slug)
		
	def markdown_filter(text):
		return ''.join(re.split(r'<hr\s*/?>', markdown.markdown(text)))


class TemplatedView(object):
	def __init__(self, folder, routes, hooks={}, auto_reload=True):
		self.env = jinja2.Environment(
			auto_reload=auto_reload,
			loader=jinja2.FileSystemLoader(os.path.join(folder, 'html'))
		)
		self.env.filters['markdown'] = TemplatedView.markdown_filter
		self.env.filters['summary'] = TemplatedView.summary_filter
		self.env.filters['img_src'] = self.img_src
		self.env.filters['atom'] = self.atom
		self.env.filters['resource'] = TemplatedView.resource

		self.routes = routes
		self.hooks = hooks

	def markdown_filter(text):
		return ''.join(re.split(r'<hr\s*/?>', markdown.markdown(text)))

	def summary_filter(text):
		return re.split(r'<hr\s*/?>', markdown.markdown(text), 1)[0]

	def atom(self, slug):
		return self.routes['atom'].format(slug=slug)

	def img_src(self, img, post, w=None, h=None):
		route = self.routes['image'].format(
			post_slug=post.slug,
			post_id=post.id,
			image=img.name
		)

		self.call_hook('img_src', post, img, w=w, h=h)
		if w and h:
			return '{}?w={}&h={}'.format(route, w, h)
		elif w:
			return '{}?w={}'.format(route, w)
		elif h:
			return '{}?h={}'.format(route, h)
		else:
			return route

	def resource(name):
		return "/resource/" + name

	def render_search_results(self, **kwargs):
		return self.env.get_template('search.html').render(**kwargs)

	def render_list(self, **kwargs):
		return self.env.get_template('list.html').render(**kwargs)

	def render_post(self, **kwargs):
		return self.env.get_template('post.html').render(**kwargs)

	def call_hook(self, hook, *vargs, **kwargs):
		hook = self.hooks.get(hook)
		if hook:
			hook(*vargs, **kwargs)
